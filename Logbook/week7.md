# Logbook Week 7


**Name :** Muhammad Aiman bin Mohammad Sabri 

**Matric No.:**  197907 

**Subsystem :**  Design and Structure 

**Group:** 6 

| Week 7 |  |
| ------ | ------ |
| **1. Agenda and goal.** | -Add features for the carrier board mount and holes for bolt and nut. <br/> -Finalise the design of the gondola. <br/> |
| **2. Problem and descision taken** | **Problem**: The gondola design was not able to present as a product.<br/><br/>**Desicions**: minor modification for the gondola has been adjusted. The previous idea of covering the gondola with mppf has neglected.<br/>- Strings and velcro tape to attach the gondola with the ariship.|
| **3. Method to solve the problems** |-Instead of making the gondola as a 'box', we decided to put v-shaped bottom layer.<br/> -The sides of the gondola were modified with adding vent for air flow and lightening purposes.<br/> |
| **4. Justification when solving problem** | -The design of the gondola was able to be seen as a product.<br/>
| **5. Impact of/after the decision chosen** | -The gondola design has been modified according to the supervisor's interest and can proceed with weight calculation.<br/> |
| **6. Next step** | -Weight calculation for the gondola with the components.<br/>-Print the gondola body in fabrication lab. <br/>|
