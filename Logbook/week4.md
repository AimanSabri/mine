# Logbook Week 4


**Name :** Muhammad Aiman bin Mohammad Sabri 

**Matric No.:**  197907 

**Subsystem :**  Design and Structure 

**Group:** 6 

| Week 4 |  |
| ------ | ------ |
| **1. Agenda and goal.** | Sketching ideas for gondola and propose the ideas to supervisor|
| **2. Problem and descision taken** | **Problem**: Previous gondola design was too heavy and not efficient in term of components placement.<br/><br/>**Desicions**: Add some features to the gondola and pick suitable material to make the gondola lighter . Moreover, 3 layers of gondola need to be considered for new components placement|
| **3. Method to solve the problems** | Adding lightening holes to the gondola, design the gondola into 3 layers as trays for accessibility. |
| **4. Justification when solving problem** | The trays structures of the interior gondola designed with 3D printed materials to secure the placement of the components. All the ideas were apporved by the supervisor during the lab visit.
| **5. Impact of/after the decision chosen** | Able to find the solution to the design problem. |
| **6. Next step** | Start to design the new gondola in CAD.|
