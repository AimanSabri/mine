# Logbook Week 5


**Name :** Muhammad Aiman bin Mohammad Sabri 

**Matric No.:**  197907 

**Subsystem :**  Design and Structure 

**Group:** 6 

| Week 5 |  |
| ------ | ------ |
| **1. Agenda and goal.** | Sketching ideas for redesigning gondola and propose the ideas to supervisor|
| **2. Problem and descision taken** | **Problem**: Previous gondola design was not accessible for inpection purposes and not efficient in term of components placement.<br/><br/>**Desicions**: Add some features to the gondola with 3 layers stacked on each other . Moreover, 3 layers of gondola need to be considered for new components placement|
| **3. Method to solve the problems** |Design the gondola into 3 layers as trays for accessibility by stacking on top of each other alongside with latch at the side of it. |
| **4. Justification when solving problem** | The sekleton structure for layers of gondola purposed to be design with 3D printed materials to secure the placement of the components we add some clip/latch on each side of the gondola. All the ideas were apporved by the supervisor during the progress report presentation.
| **5. Impact of/after the decision chosen** | Able to find the solution to the design problem and approved by the lecturer. |
| **6. Next step** | Start to design the new gondola in CAD.|
