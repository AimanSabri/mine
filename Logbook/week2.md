# Logbook Week 2


**Name :** Muhammad Aiman bin Mohammad Sabri 

**Matric No.:**  197907 

**Subsystem :**  Design and Structure 

**Group:** 6 

| Week 2 |  |
| ------ | ------ |
| **1. Agenda and goal.** | Conducting a group discussion about the project milestone and gantt chart for the Design and Structure subsystem. Moreover, getting familiarise with gitlab and visual code for project documentation and progress. By the end of the week, the goal is to propose a suitable gantt chart that include tasks and its duration for each week along project milestone.|
| **2. Problem and descision taken** | **Problem**: The design of the thruster arm and gondola are considered as not stable.<br/><br/>**Desicions**: Divide into 2 teams each for thruster arm and gondola design. As a team tasked to gondola design, we manage to create online platform discussion for further progress.|
| **3. Method to solve the problems** | We manage to split the big team into 2 each for gondola and thruster arm. Further discussions were made through online platform such as WhatsApp and Discord. Brainstorming session for the gondola team conducted to find solution to the problem.  |
| **4. Justification when solving problem** | We split into two design groups in order to meet the deadline and maximise our group functions, with everyone in the group contributing to their designated group, which they had chosen on their own.
| **5. Impact of/after the decision chosen** | It is easier for members to communicate with one another, and each member can offer their own ideas for improving the specifications of the new gondola and thruster arm. |
| **6. Next step** | Next week, the members will start to sketch the design of both gondola and thruster arm. The members would also visit the lab to take a look at the old gondola and thruster arm designed by the seniors. Hence, determine the needs to improve some of their features.|
