# Logbook Week 6


**Name :** Muhammad Aiman bin Mohammad Sabri 

**Matric No.:**  197907 

**Subsystem :**  Design and Structure 

**Group:** 6 

| Week 6 |  |
| ------ | ------ |
| **1. Agenda and goal.** | -Visitng the lab to discuss with the supervisor upon the design of the gondola. <br/> -Finding the stregths and weaknesses for our gondola. <br/> -Discussion upon the suitable material to withstand the load of the payload.|
| **2. Problem and descision taken** | **Problem**: The gondola design was not efficient in term of weigth distribution.<br/><br/>**Desicions**: The 3 layers of gondola were reduced to 2 layers as the layout of battery and esc were both included in the same layer . Moreover, support such as beam has been added to the side of the gondola to make it stronger.|
| **3. Method to solve the problems** |The 3 layers of gondola were reduced to 2 layers as the layout of battery and esc were both included in the same layer . Moreover, support such as beam has been added to the side of the gondola to make it stronger. The placement of the carrier board will be inverted at the top layer for accessibility. |
| **4. Justification when solving problem** | The new design with 2 layers will help to reduce the weight and improvise the accesibility for all components in the gondola itself.
| **5. Impact of/after the decision chosen** | Able to find the solution to the design problem and approved by the lecturer. |
| **6. Next step** | -Decide upon the attachment of the gondola to the airship.<br/>-Find the suitable clamp for the payload attachment. <br/>-Add features for carrier board mount and holes for bolt and nut.|
